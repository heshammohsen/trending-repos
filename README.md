# Trending-repos

list the most starred Github repos that were created in the last 30 days.

## Tech

**Trending-repos** uses a number of open source projects to work properly:

- [ReactJs](https://reactjs.org/) - A JavaScript library for building user interfaces
- [Bootstrap](https://getbootstrap.com/) - Build fast, responsive sites with Bootstrap

### Development

Open your favorite Terminal and run these commands.

```sh
$ npm start
```

## Authors

**Hesham Mohsen.**
